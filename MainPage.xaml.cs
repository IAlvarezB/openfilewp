﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using OpenFile.Resources;
using System.IO;
using System.Threading.Tasks;

namespace OpenFile
{
    public partial class MainPage : PhoneApplicationPage
    {

        FileInfo fileInfo;
        long fileLength;
        String texto;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            fileInfo = new FileInfo("Assets/Files/educacion_fundamental.txt");
            fileLength = fileInfo.Length;
            progressBar.Maximum = fileLength;
           
        }

        private void cargarFichero(object sender, RoutedEventArgs e)
        {

            ReadFileAsync();
        }

    
        private async void ReadFileAsync()
        {
            String line;
            int currentPosition = 0;

            StreamReader sr = new StreamReader("Assets/Files/educacion_fundamental.txt", System.Text.Encoding.GetEncoding("iso-8859-1"));

          
                while ((line = await sr.ReadLineAsync()) != null)
                {
                    currentPosition += line.Length + 2; // or plus 2 if you need to take into account carriage return
                    int value = (int)(((decimal)currentPosition / (decimal)fileLength) * (decimal)100);
                    
                    progressBar.Value = currentPosition;// value;
                    textBlock.Text = value.ToString() + "%";

                texto += line;
                
                }

            textoFichero.Text = texto;
        }
       
    }
}